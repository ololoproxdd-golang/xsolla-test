Xsolla тестовое задание - сервис сокращатель ссылок
--
### Необходимые приложения и пакеты
Для корректной работы приложения требуется:
1. Сервер с [базой данных Postgres](https://www.postgresql.org/download/)
2. [Компилятор Golang](https://golang.org/dl/)
3. [Менеджер зависимостей dep](https://golang.github.io/dep/)
```go get -u github.com/golang/dep/cmd/dep```
4. [Пакет goswagger](https://goswagger.io/)
```go get -u github.com/go-swagger/go-swagger/cmd/swagger```

Также для взаимодействия с API я использовал [Postman](https://www.getpostman.com/)

### Сборка
1. Генерируем сервер ```swagger generate server -f ./swagger.yml```
2. Подтягиваем зависимости ```dep init```
3. В файле `config.json` необходимо настроить переменные для работы с БД 
4. Устанавливаем сервер ```go install ./cmd/url-shortening-service-server/```

### Запуск
1. ```url-shortening-service-server --host=127.0.0.1 --port=8080```
где в качестве host и port можно указать свои значения.

Документация - `http://host:port/docs`

Все произошедшие в системы события записвыаются в файл `log` и в консоль.