package app_services

import (
	"database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"sort"
	"strconv"
	"time"
	"xsolla-test/app/app_db"
	"xsolla-test/app/app_db/db_models"
	"xsolla-test/models"
)

type HistoryService struct {
	Db *gorm.DB
}

var historyServiceInst *HistoryService = nil

func GetHistoryService() *HistoryService {
	if historyServiceInst == nil {

		dbContext, err := app_db.GetDbContext()
		if err != nil {
			log.Error().Err(err).Msg("dbContext init error")
			return nil
		}

		historyServiceInst = &HistoryService{Db: dbContext.Db}
		historyServiceInst.init()
	}

	return historyServiceInst
}

func (service *HistoryService) init() {
	service.Db.AutoMigrate(&db_models.History{})
}

func (service *HistoryService) AddToHistory(linkId int64) int64 {
	now := time.Now().UTC()
	dbHistory := &db_models.History{LinkId: linkId, Ts: now}
	service.Db.Create(&dbHistory)
	return dbHistory.ID
}

func (service *HistoryService) Top20() ([]*models.LinkDetailResponse, error) {
	rows, err := service.Db.Table("history").Group("link_id").Limit(20).Select("link_id, count(*) ").Rows()

	if err != nil {
		log.Error().Err(err)
		return nil, err
	}

	var response []*models.LinkDetailResponse
	for rows.Next() {
		var linkId, count int64
		err := rows.Scan(&linkId, &count)
		if err != nil {
			log.Error().Err(err)
			break
		}

		if link := GetLinkService().GetById(linkId); link != nil {
			el := &models.LinkDetailResponse{
				ID:         &link.ID,
				Long:       &link.Long,
				ReferCount: &count,
				Short:      &link.Short,
			}
			response = append(response, el)
		}
	}

	sort.Slice(response, func(i, j int) bool {
		return *response[i].ReferCount > *response[j].ReferCount
	})

	return response, nil
}

func (service *HistoryService) Stats() (*models.LinkStatsResponse, error) {
	rows, err := service.Db.Table("history").Group("t").Select("date_trunc('minute', ts) t, count(*)").Rows()
	defer func() {
		err = rows.Close()
		if err != nil {
			log.Error().Err(err).Msgf("rows close error")
		}
	}()

	if err != nil {
		log.Error().Err(err)
		return nil, errors.Wrap(err, "sql raw error")
	}

	return service.parseLinkStats(rows)
}

func (service *HistoryService) parseLinkStats(rows *sql.Rows) (*models.LinkStatsResponse, error) {
	result := &models.LinkStatsResponse{Days: models.Days{}}
	var dbTime time.Time
	var count int64
	for rows.Next() {
		err := rows.Scan(&dbTime, &count)
		if err != nil {
			log.Error().Err(err)
			return nil, errors.Wrap(err, "sql scan row error")
		}

		utc := dbTime.UTC()
		sDay := fmt.Sprintf("%d.%d.%d", utc.Day(), utc.Month(), utc.Year())
		days := &result.Days
		dayId, found := findDay(days, &sDay)
		if !found {
			dayEl := &models.Day{Hours: models.Hours{}, Value: sDay}
			dayId = len(*days)
			*days = append(*days, dayEl)
		}

		sHour := strconv.Itoa(utc.Hour()) + "h"
		hours := &(*days)[dayId].Hours
		hourId, found := findHour(hours, &sHour)
		if !found {
			hourEl := &models.Hour{Minutes: models.Minutes{}, Value: sHour}
			hourId = len(*hours)
			*hours = append(*hours, hourEl)
		}

		sMinute := strconv.Itoa(utc.Minute()) + "min"
		minutes := &(*hours)[hourId].Minutes
		minuteEl := &models.Minute{Value: sMinute, Count: count}
		*minutes = append(*minutes, minuteEl)
	}

	return result, nil
}

func findDay(days *models.Days, day *string) (int, bool) {
	for idx, val := range *days {
		if val.Value == *day {
			return idx, true
		}
	}
	return 0, false
}

func findHour(hours *models.Hours, hour *string) (int, bool) {
	for idx, val := range *hours {
		if val.Value == *hour {
			return idx, true
		}
	}
	return 0, false
}
