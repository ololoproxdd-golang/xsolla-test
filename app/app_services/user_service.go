package app_services

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/bcrypt"
	"xsolla-test/app/app_db"
	"xsolla-test/app/app_db/db_models"
	"xsolla-test/models"
	"xsolla-test/restapi/operations/user"
)

type UserService struct {
	Db *gorm.DB
}

func (service *UserService) Init() {
	service.Db.AutoMigrate(&db_models.User{})
}

var userServiceInst *UserService = nil

func GetUserService() *UserService {
	if userServiceInst == nil {

		dbContext, e := app_db.GetDbContext()
		if e != nil {
			log.Error().Err(e).Msg("dbContext init error")
			return nil
		}

		userServiceInst = &UserService{dbContext.Db}
		userServiceInst.Init()
	}

	return userServiceInst
}

func (service *UserService) Registration(params user.UserRegParams) error {
	request := params.Body
	if request == nil {
		return errors.New("body nil")
	}

	var dbUsers []db_models.User
	var count int64
	service.Db.Where("username = ?", *request.Username).Find(&dbUsers).Count(&count)
	if count > 0 {
		return errors.New("user with this name already exist")
	}

	saltPw, err := service.hashAndSalt([]byte(*request.Password))
	if err != nil {
		return errors.Wrap(err, "password hash salt error")
	}
	dbUser := &db_models.User{Username: *request.Username, Password: saltPw}
	service.Db.Create(dbUser)
	return nil
}

func (service *UserService) Detail(id int64) *models.UserDetailResponse {
	dbUser := &db_models.User{}
	service.Db.Find(&dbUser, id)
	response := &models.UserDetailResponse{ID: &dbUser.ID, Name: &dbUser.Username}
	return response
}

func (service *UserService) CheckDbUser(username string, password string) (int64, error) {
	dbUser := &db_models.User{}
	service.Db.Where("username = ?", username).First(&dbUser)
	var userResponseId int64
	err := service.comparePasswords(dbUser.Password, []byte(password))
	if err == nil {
		userResponseId = dbUser.ID
	}
	return userResponseId, err
}

func (service *UserService) comparePasswords(hashedPwd string, plainPwd []byte) error {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		sErr := "comparePasswords error"
		log.Error().Err(err).Msgf(sErr)
		return errors.Wrap(err, sErr)
	}

	return nil
}

func (service *UserService) hashAndSalt(pwd []byte) (string, error) {

	// Use GenerateFromPassword to hash & salt pwd
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		sErr := "hashAndSalt error"
		log.Error().Err(err).Msgf(sErr)
		return "", errors.Wrap(err, sErr)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash), nil
}
