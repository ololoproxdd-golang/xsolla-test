package app_services

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"os"
	"time"
)

func InitZeroLog() error {
	zerolog.TimeFieldFormat = time.Stamp
	zerolog.MessageFieldName = "MESSAGE"
	zerolog.LevelFieldName = "LEVEL"
	zerolog.TimestampFieldName = "TIME"

	f, err := os.OpenFile("log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Error().Msg("log file open error")
		return err
	}
	mw := io.MultiWriter(os.Stderr, f)

	log.Logger = zerolog.New(mw).With().Timestamp().Logger()

	return nil
}
