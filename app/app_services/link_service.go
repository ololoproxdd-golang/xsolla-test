package app_services

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
	"xsolla-test/app/app_db"
	"xsolla-test/app/app_db/db_models"
	"xsolla-test/models"
	"xsolla-test/restapi/operations/link"
)

type LinkService struct {
	Db *gorm.DB
}

var linkServiceInst *LinkService = nil

func GetLinkService() *LinkService {
	if linkServiceInst == nil {

		dbContext, e := app_db.GetDbContext()
		if e != nil {
			log.Error().Err(e).Msg("dbContext init error")
			return nil
		}

		linkServiceInst = &LinkService{dbContext.Db}
		linkServiceInst.Init()
	}

	return linkServiceInst
}

func (service *LinkService) Init() {
	service.Db.AutoMigrate(&db_models.Link{})
}

func (service *LinkService) Create(params link.LinkCreateParams, userId int64) *models.LinkCreateResponse {
	request := params.Body
	var dbLink db_models.Link
	var count int64
	response := &models.LinkCreateResponse{}

	if service.Db.Where("long = ? and user_id = ?", *request.Long, userId).First(&dbLink).Count(&count); count > 0 {
		response.Short = &dbLink.Short
	} else {
		dbLink = db_models.Link{Long: *request.Long, UserId: userId}
		service.Db.Create(&dbLink)
		dbLink.Short = Encode(uint64(dbLink.ID))
		service.Db.Save(&dbLink)
		response.Short = &dbLink.Short
	}
	return response
}

func (service *LinkService) Delete(linkShort string, userId int64) bool {
	var dbLink db_models.Link
	var count int64
	service.Db.Where("short = ? and user_id = ?", linkShort, userId).First(&dbLink).Count(&count)
	if count > 0 {
		service.Db.Delete(dbLink)
		return true
	}

	return false
}

func (service *LinkService) Detail(linkShort string, userId int64) *models.LinkDetailResponse {
	var dbLink db_models.Link
	var count int64
	service.Db.Where("short = ? and user_id = ?", linkShort, userId).First(&dbLink).Count(&count)
	if count > 0 {
		response := &models.LinkDetailResponse{ID: &dbLink.ID, Short: &dbLink.Short, Long: &dbLink.Long, ReferCount: &dbLink.ReferCount}
		return response
	}

	return nil
}

func (service *LinkService) GetAllUserLinks(userId int64) []*models.LinkDetailResponse {
	var dbLinks []*db_models.Link
	var count int64
	if service.Db.Where("user_id = ?", userId).Find(&dbLinks).Count(&count); count > 0 {
		var response []*models.LinkDetailResponse
		for _, dbLink := range dbLinks {
			responseEl := &models.LinkDetailResponse{ID: &dbLink.ID, Short: &dbLink.Short, Long: &dbLink.Long, ReferCount: &dbLink.ReferCount}
			response = append(response, responseEl)
		}
		return response
	}
	return nil
}

func (service *LinkService) Redirect(linkShort string) (*db_models.Link, bool) {
	var dbLink db_models.Link
	var count int64
	service.Db.Where("short = ?", linkShort).First(&dbLink).Count(&count)
	if count > 0 {
		dbLink.ReferCount++
		service.Db.Save(&dbLink)
		return &dbLink, true
	}

	return nil, false
}

func (service *LinkService) GetById(id int64) *db_models.Link {
	var dbLink db_models.Link
	var count int64
	service.Db.Where("id = ? and short <> ?", id, "").First(&dbLink).Count(&count)
	if count > 0 {
		return &dbLink
	}
	return nil
}
