package app_http

import (
	errs "github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"xsolla-test/app/app_services"
	"xsolla-test/models"
	"xsolla-test/restapi/operations"
	"xsolla-test/restapi/operations/link"
	"xsolla-test/restapi/operations/redirect"
	"xsolla-test/restapi/operations/user"
)

type HttpHandler struct {
	userService    *app_services.UserService
	historyService *app_services.HistoryService
	linkService    *app_services.LinkService
}

var httpHandlerInst *HttpHandler = nil

func GetHttpHandler() *HttpHandler {
	if httpHandlerInst == nil {
		httpHandlerInst = &HttpHandler{}
		if err := httpHandlerInst.init(); err != nil {
			httpHandlerInst = nil
		}
	}

	return httpHandlerInst
}

func (handler *HttpHandler) init() error {
	if handler.historyService = app_services.GetHistoryService(); handler.historyService == nil {
		return errors.New("historyService not init")
	}

	if handler.userService = app_services.GetUserService(); handler.userService == nil {
		return errors.New("userService not init")
	}

	if handler.linkService = app_services.GetLinkService(); handler.linkService == nil {
		return errors.New("linkService not init")
	}

	return nil
}

func (handler *HttpHandler) errResponse(code int, message string) *models.ErrorResponse {
	errResponse := &models.ErrorResponse{}
	errResponse.Code = new(int64)
	*errResponse.Code = int64(code)
	errResponse.Message = new(string)
	*errResponse.Message = message
	return errResponse
}

func (handler *HttpHandler) LinkCreateHandler(params link.LinkCreateParams, principal interface{}) middleware.Responder {
	if userId, ok := principal.(int64); ok && &handler.linkService != nil {
		response := handler.linkService.Create(params, userId)
		log.Info().Msgf("LinkCreateHandler: link %s create", *params.Body.Long)
		return link.NewLinkCreateCreated().WithPayload(response)
	}

	errCode := link.LinkCreateBadRequestCode
	errMessage := "LinkCreateHandler: link create error"
	log.Error().Msgf(errMessage)
	return link.NewLinkCreateBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) LinkDeleteHandler(params link.LinkDeleteParams, principal interface{}) middleware.Responder {
	if userId, ok := principal.(int64); ok && &handler.linkService != nil {
		deleted := handler.linkService.Delete(params.LinkShort, userId)
		if deleted {
			log.Info().Msgf("LinkDeleteHandler: link %s deleted", params.LinkShort)
			return link.NewLinkDeleteNoContent()
		}

		errCode := link.LinkDeleteNotFoundCode
		errMessage := "LinkDeleteHandler: link not found"
		log.Error().Msgf(errMessage)
		return link.NewLinkDeleteNotFound().WithPayload(handler.errResponse(errCode, errMessage))
	}

	errCode := link.LinkDeleteBadRequestCode
	errMessage := "LinkDeleteHandler: link delete error"
	log.Error().Msgf(errMessage)
	return link.NewLinkDeleteBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) LinkDetailHandler(params link.LinkDetailParams, principal interface{}) middleware.Responder {
	if userId, ok := principal.(int64); ok && &handler.linkService != nil {
		response := handler.linkService.Detail(params.LinkShort, userId)
		if response != nil {
			log.Info().Msgf("LinkDetailHandler: detail for link %s", params.LinkShort)
			return link.NewLinkDetailOK().WithPayload(response)
		}

		errCode := link.LinkDetailNotFoundCode
		errMessage := "LinkDetailHandler: link not found"
		log.Error().Msgf(errMessage)
		return link.NewLinkDetailNotFound().WithPayload(handler.errResponse(errCode, errMessage))
	}

	errCode := link.LinkDetailBadRequestCode
	errMessage := "LinkDetailHandler: link detail error"
	log.Error().Msgf(errMessage)
	return link.NewLinkDetailBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) LinkStatsHandler(params link.LinkStatsParams, principal interface{}) middleware.Responder {
	errCode := link.LinkStatsBadRequestCode
	if &handler.historyService != nil {
		response, err := handler.historyService.Stats()
		if err != nil {
			errMessage := err.Error()
			log.Error().Msgf("LinkStatsHandler: " + errMessage)
			return link.NewLinkStatsBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
		}

		log.Info().Msgf("LinkStatsHandler: time schedule for the number of transitions with grouping by day, hours, minutes")
		return link.NewLinkStatsOK().WithPayload(response)
	}

	errMessage := "LinkStatsHandler: historyService is nil"
	log.Error().Msgf(errMessage)
	return link.NewLinkStatsBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) LinkTop20Handler(params link.LinkTop20Params, principal interface{}) middleware.Responder {
	errCode := link.LinkTop20BadRequestCode
	if &handler.historyService != nil {
		response, err := handler.historyService.Top20()
		if err != nil {
			errMessage := "LinkTop20Handler: " + err.Error()
			log.Error().Msgf(errMessage)
			return link.NewLinkTop20BadRequest().WithPayload(handler.errResponse(errCode, errMessage))
		}

		log.Info().Msgf("LinkTop20Handler: success received top links")
		return link.NewLinkTop20OK().WithPayload(response)
	}

	errMessage := "LinkTop20Handler: historyService is nil"
	log.Error().Msgf(errMessage)
	return link.NewLinkTop20BadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) UserRegistrationHandler(params user.UserRegParams) middleware.Responder {
	errCode := user.UserRegBadRequestCode
	if &handler.userService != nil {
		err := handler.userService.Registration(params)
		if err != nil {
			errMessage := err.Error()
			log.Error().Msgf("UserRegistrationHandler: " + errMessage)
			return user.NewUserRegBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
		}

		log.Info().Msgf("UserRegistrationHandler: user %s success registration", *params.Body.Username)
		return user.NewUserRegCreated()
	}

	errMessage := "UserRegistrationHandler: userService is nil"
	log.Error().Msgf(errMessage)
	return user.NewUserRegBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) UserDetailHandler(params user.UserDetailParams, principal interface{}) middleware.Responder {
	errCode := user.UserDetailBadRequestCode
	if &handler.userService != nil {
		if userId, ok := principal.(int64); ok {
			response := handler.userService.Detail(userId)
			log.Info().Msgf("UserDetailHandler: detail for user %s", response.Name)
			return user.NewUserDetailOK().WithPayload(response)
		}
	}

	errMessage := "UserDetailHandler: userService is nil"
	log.Error().Msgf(errMessage)
	return user.NewUserDetailBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) UserLinksHandler(params operations.UserLinksParams, principal interface{}) middleware.Responder {
	errCode := user.UserDetailBadRequestCode
	if &handler.linkService != nil {
		if userId, ok := principal.(int64); ok {
			response := handler.linkService.GetAllUserLinks(userId)
			log.Info().Msgf("UserLinksHandler: user with id %d links", userId)
			return operations.NewUserLinksOK().WithPayload(response)
		}
	}

	errMessage := "UserLinksHandler: linkService is nil"
	log.Error().Msgf(errMessage)
	return operations.NewUserLinksBadRequest().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) LinkRedirectHandler(params redirect.LinkRedirectParams) middleware.Responder {
	errCode := operations.UserLinksBadRequestCode
	if &handler.linkService != nil && &handler.historyService != nil {
		if dbLink, isRedirect := handler.linkService.Redirect(params.LinkShort); isRedirect {
			handler.historyService.AddToHistory(dbLink.ID)
			log.Info().Msgf("LinkRedirectHandler: redirect from %s to %s success", params.LinkShort, dbLink.Long)
			return redirect.NewLinkRedirectFound().WithLocation(dbLink.Long)
		}

		errMessage := "LinkRedirectHandler: link not found"
		log.Error().Msgf(errMessage)
		return redirect.NewLinkRedirectNotFound().WithPayload(handler.errResponse(errCode, errMessage))
	}

	errMessage := "LinkRedirectHandler: linkService or historyService is nil"
	log.Error().Msgf(errMessage)
	return redirect.NewLinkRedirectNotFound().WithPayload(handler.errResponse(errCode, errMessage))
}

func (handler *HttpHandler) BasicAuthHandler(user string, pass string) (interface{}, error) {
	if &handler.userService != nil {
		if userId, err := handler.userService.CheckDbUser(user, pass); err == nil {
			log.Info().Msgf("BasicAuthHandler: auth success for %s", user)
			return userId, nil
		}

		log.Error().Msgf("BasicAuthHandler: auth error for %s", user)
		return nil, errs.Unauthenticated(user)
	}

	log.Error().Msgf("BasicAuthHandler: auth error for %s. userService is nil", user)
	return nil, errs.Unauthenticated(user)
}
