package app_http

import (
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

func AccessLogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info().Msgf("accessLogMiddleware %s", r.URL.Path)
		start := time.Now().UTC()
		next.ServeHTTP(w, r)
		log.Info().Msgf("[%s] %s, %s %s",
			r.Method, r.RemoteAddr, r.URL.Path, time.Since(start))
	})
}

func PanicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info().Msgf("panicMiddleware %s", r.URL.Path)
		defer func() {
			if err := recover(); err != nil {
				switch x := err.(type) {
				case string:
					log.Error().Msgf("error %s", x)
				case error:
					log.Error().Msgf("error %s", x.Error())
				default:
					log.Error().Msgf("unknown error")
				}
				http.Error(w, "Internal server error", 500)
			}
		}()
		next.ServeHTTP(w, r)
	})
}
