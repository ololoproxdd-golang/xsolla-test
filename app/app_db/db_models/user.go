package db_models

type User struct {
	ID       int64
	Username string `gorm:"not null;unique"`
	Password string `gorm:"not null"`
}
