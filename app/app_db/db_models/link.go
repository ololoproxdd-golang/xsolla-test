package db_models

type Link struct {
	ID         int64
	Short      string
	Long       string `gorm:"not null"`
	UserId     int64  `gorm:"not null"`
	ReferCount int64  `gorm:"not null"`
}
