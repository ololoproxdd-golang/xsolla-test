package db_models

import "time"

type History struct {
	ID     int64
	LinkId int64     `gorm:"not null"`
	Ts     time.Time `gorm:"not null"`
}
