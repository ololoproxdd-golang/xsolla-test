package app_db

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
)

type DbConfig struct {
	PostgresDb struct {
		Host     string
		Port     string
		User     string
		Password string
		DbName   string
		LogMode  string
	}
}

var dbConfigInst *DbConfig = nil

func GetDbConfig() (*DbConfig, error) {
	if dbConfigInst == nil {
		dbConfigInst = &DbConfig{}
		err := dbConfigInst.unmarshalJsonConfig()
		if err != nil {
			return nil, errors.Wrap(err, "unmarshal json error")
		}
	}

	return dbConfigInst, nil
}

func (config *DbConfig) unmarshalJsonConfig() error {
	data, err := ioutil.ReadFile("./config.json")
	if err != nil {
		return errors.Wrap(err, "read file error")
	}

	err = json.Unmarshal(data, config)
	if err != nil {
		return errors.Wrap(err, "parse json error")
	}

	return nil
}

func (config *DbConfig) GetDbHost() string {
	return config.PostgresDb.Host
}
func (config *DbConfig) GetDbPort() string {
	return config.PostgresDb.Port
}
func (config *DbConfig) GetDbUser() string {
	return config.PostgresDb.User
}
func (config *DbConfig) GetDbPassword() string {
	return config.PostgresDb.Password
}
func (config *DbConfig) GetDbName() string {
	return config.PostgresDb.DbName
}
func (config *DbConfig) GetLogMode() string {
	return config.PostgresDb.LogMode
}
