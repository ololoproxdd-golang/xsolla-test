package app_db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type DbContext struct {
	Config DbConfig
	Db     *gorm.DB
}

var dbInst *DbContext = nil

func GetDbContext() (*DbContext, error) {
	if dbInst == nil {
		dbConfig, err := GetDbConfig()
		if err != nil {
			return nil, errors.Wrap(err, "dbContext config error")
		}

		dbInst = &DbContext{Config: *dbConfig}
		err = dbInst.initDb()
		if err != nil {
			dbInst = nil
			return nil, errors.Wrap(err, "dbContext init error")
		}
	}

	return dbInst, nil
}

func (context *DbContext) initDb() error {
	err := context.connectDb()
	if err != nil {
		return err
	}

	logMode := context.Config.GetLogMode() == "true"
	context.Db.LogMode(logMode)
	context.Db.SetLogger(&log.Logger)
	context.Db.SingularTable(true)
	return nil
}

func (context *DbContext) connectDb() error {
	cfg := &context.Config
	connectionString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s",
		cfg.GetDbHost(), cfg.GetDbPort(), cfg.GetDbUser(), cfg.GetDbName(), cfg.GetDbPassword())
	db, err := gorm.Open("postgres", connectionString)

	if err != nil {
		return errors.Wrap(err, "database connection error")
	}
	context.Db = db
	return nil
}

func (context *DbContext) CloseDb() error {
	err := context.Db.Close()
	if err != nil {
		return errors.Wrap(err, "database close error")
	}
	return nil
}
