// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"fmt"
	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/rs/zerolog/log"
	"net/http"
	"xsolla-test/app/app_db"
	"xsolla-test/app/app_http"
	"xsolla-test/app/app_services"
	"xsolla-test/restapi/operations/redirect"

	"xsolla-test/restapi/operations"
	"xsolla-test/restapi/operations/link"
	"xsolla-test/restapi/operations/user"
)

func configureFlags(api *operations.URLShorteningServiceAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

var httpHandler *app_http.HttpHandler

func configureAPI(api *operations.URLShorteningServiceAPI) http.Handler {

	// configure the api here
	api.ServeError = errors.ServeError

	api.Logger = log.Info().Msgf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	httpHandler = app_http.GetHttpHandler()
	api.LinkLinkCreateHandler = link.LinkCreateHandlerFunc(httpHandler.LinkCreateHandler)
	api.LinkLinkDeleteHandler = link.LinkDeleteHandlerFunc(httpHandler.LinkDeleteHandler)
	api.LinkLinkDetailHandler = link.LinkDetailHandlerFunc(httpHandler.LinkDetailHandler)
	api.LinkLinkStatsHandler = link.LinkStatsHandlerFunc(httpHandler.LinkStatsHandler)
	api.LinkLinkTop20Handler = link.LinkTop20HandlerFunc(httpHandler.LinkTop20Handler)
	api.UserUserRegHandler = user.UserRegHandlerFunc(httpHandler.UserRegistrationHandler)
	api.UserUserDetailHandler = user.UserDetailHandlerFunc(httpHandler.UserDetailHandler)
	api.UserLinksHandler = operations.UserLinksHandlerFunc(httpHandler.UserLinksHandler)
	api.BasicAuthAuth = httpHandler.BasicAuthHandler
	api.RedirectLinkRedirectHandler = redirect.LinkRedirectHandlerFunc(httpHandler.LinkRedirectHandler)

	api.ServerShutdown = func() {
		if db, err := app_db.GetDbContext(); err != nil {
			log.Error().Err(err)
		} else {
			if err = db.CloseDb(); err != nil {
				log.Error().Err(err)
			}
		}
	}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
	e := app_services.InitZeroLog()
	if e != nil {
		fmt.Println("zerolog init error")
	}

	if httpHandler == nil {
		log.Error().Msg("http handler is nil")
		err := s.Shutdown(nil)
		log.Error().Err(err)
	}

}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	//handler = services.BasicAuthMiddleware(handler)
	handler = app_http.AccessLogMiddleware(handler)
	handler = app_http.PanicMiddleware(handler)
	return handler
}
