consumes:
  - application/json
info:
  description: Test assignment for the interview in Xsolla
  title: URL shortening service
  version: 1.0.0

paths:
  /user/reg:
    post:
      description: User registration
      tags:
        - user
      operationId: userReg
      parameters:
        - in: body
          name: body
          schema:
            $ref: '#/definitions/UserRegRequest'
      responses:
        '201':
          description: User registered successfully
        '400':
          description: Registration error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /user/detail:
    get:
      description: Getting information about the current authorized user. Authorization required
      security:
        - basicAuth: []
      tags:
        - user
      operationId: userDetail
      responses:
        '200':
          description: Successfully retrieving user information
          schema:
            $ref: "#/definitions/UserDetailResponse"
        '400':
          description: User detail error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /link/create:
    post:
      description: Creating a new short link. Authorization required
      security:
        - basicAuth: []
      tags:
        - link
      operationId: linkCreate
      parameters:
        - in: body
          name: body
          schema:
            $ref: '#/definitions/LinkCreateRequest'
      responses:
        '201':
          description: Successful creation of a new link
          schema:
            $ref: "#/definitions/LinkCreateResponse"
        '400':
          description: Create link error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /user/links:
    get:
      description: Getting all created short user links. Authorization required
      security:
        - basicAuth: []
      tags:
        - user
        - link
      operationId: userLinks
      responses:
        '200':
          description: Successful receipt of all short user links
          schema:
            type: array
            items:
              $ref: "#/definitions/LinkDetailResponse"
        '400':
          description: Errors in getting all short user links
          schema:
            $ref: "#/definitions/ErrorResponse"

  /link/{link-short}+:
    get:
      description: Getting information about a specific short user link. Returns an error if the user does not own the link. Authorization required
      security:
        - basicAuth: []
      tags:
        - link
      operationId: linkDetail
      parameters:
        - in: path
          name: link-short
          required: true
          type: string
      responses:
        '200':
          description: Successful receipt of information about the link
          schema:
            $ref: "#/definitions/LinkDetailResponse"
        '400':
          description: Search error
          schema:
            $ref: "#/definitions/ErrorResponse"
        '404':
          description: Link not found
          schema:
            $ref: "#/definitions/ErrorResponse"
    delete:
      description: Delete user short link. Returns an error if the user does not own the link. Authorization required
      security:
        - basicAuth: []
      tags:
        - link
      operationId: linkDelete
      parameters:
        - in: path
          name: link-short
          required: true
          type: string
      responses:
        '204':
          description: Short link success deleted
        '400':
          description: Error deleting short link
          schema:
            $ref: "#/definitions/ErrorResponse"
        '404':
          description: Link not found
          schema:
            $ref: "#/definitions/ErrorResponse"

  /link/stats:
    get:
      description: Getting a time schedule of the number of transitions with grouping by day, hour, minute. Authorization required
      security:
        - basicAuth: []
      tags:
        - link
      operationId: linkStats
      responses:
        '200':
          description: Successful receipt time schedule
          schema:
            $ref: "#/definitions/LinkStatsResponse"
        '400':
          description: Receipt error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /link/top20:
    get:
      description: Getting top from 20 referring sites. Authorization required
      security:
        - basicAuth: []
      tags:
        - link
      operationId: linkTop20
      responses:
        '200':
          description: Successful receipt top 20
          schema:
            type: array
            items:
              $ref: "#/definitions/LinkDetailResponse"
        '400':
          description: Receipt error
          schema:
            $ref: "#/definitions/ErrorResponse"

  /{link-short}:
    get:
      description: Getting a redirect to the full URL for a short link
      tags:
        - redirect
      operationId: linkRedirect
      parameters:
        - in: path
          name: link-short
          required: true
          type: string
      responses:
        '302':
          headers:
            Location:
              type: string
          description: Successful redirect
        '404':
          description: Link not found
          schema:
            $ref: "#/definitions/ErrorResponse"

definitions:
  #requests
  LinkCreateRequest:
    type: object
    required:
      - long
    properties:
      long:
        type: string

  UserRegRequest:
    type: object
    required:
      - username
      - password
    properties:
      username:
        type: string
      password:
        type: string

  #responses
  ErrorResponse:
    type: object
    required:
      - code
      - message
    properties:
      code:
        type: integer
      message:
        type: string

  UserDetailResponse:
    type: object
    required:
      - id
      - name
    properties:
      id:
        type: integer
      name:
        type: string

  LinkCreateResponse:
    type: object
    required:
      - short
    properties:
      short:
        type: string

  LinkDetailResponse:
    type: object
    required:
      - id
      - short
      - long
      - refer-count
    properties:
      id:
        type: integer
      short:
        type: string
      long:
        type: string
      refer-count:
        type: integer

  #LinkStatsResponse definitions
  LinkStatsResponse:
    type: object
    properties:
      days:
        $ref: '#/definitions/Days'

  Days:
    type: array
    items:
      $ref: '#/definitions/Day'

  Day:
    type: object
    properties:
      value:
        type: string
      hours:
        $ref: '#/definitions/Hours'

  Hours:
    type: array
    items:
      $ref: '#/definitions/Hour'

  Hour:
    type: object
    properties:
      value:
        type: string
      minutes:
        $ref: '#/definitions/Minutes'

  Minutes:
    type: array
    items:
      $ref: '#/definitions/Minute'

  Minute:
    type: object
    properties:
      value:
        type: string
      count:
        type: integer

produces:
  - application/json
schemes:
  - http
swagger: "2.0"
securityDefinitions:
  basicAuth:
    description: HTTP Basic Authorization, where the parameter is a pair base64 login-password.
    type: basic